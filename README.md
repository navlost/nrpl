# Introduction to NRPL

## Description

NRPL is a stack-based language heavily inspired by Hewlett-Packard's RPL (from which it derives its name) and Forth. It was developed to give users the ability to do complex processing on their data server- or client-side in a concise and secure manner.

Typical uses of NRPL are for querying, filtering, and transforming data. The language is general purpose, making it suitable for a variety of uses. This module was developed to fulfil the need for an interpreted language that could be embedded in JavaScript applications to run untrusted, user-supplied code. For a full description of the language and its various options and commands, please refer to its [complete specification](./doc/nrpl.md "NRPL specification").

## How it works

NRPL programs are sequences of [commands](./doc/nrpl.md#Commands), [literals](./doc/nrpl.md#Data-Types), and [variables](./doc/nrpl.md#Variables), which take arguments from, and return results to, a [stack<sup>[PDF]</sup>](http://www.sigcis.org/files/A%20brief%20history.pdf "Henriksson, Sten. “A brief history of the stack.” HAPOC11 History and Philosophy of Computing, Ghent, Belgium (2011)."). Being based on Hewlett-Packard's [RPN](http://hpmuseum.org/rpn.htm "Reverse Polish Notation"), it uses a postfix notation.

### An example

Assume you would like to add the numbers 3 and 5 together:

**Input**
```
3 5 +
```

**Output**
```
   1:   8
```

Where `1:` refers to the bottommost stack level and `8` is the result of the operation.

### Another example (step by step)

Let us take a slightly more involved example, and calculate the volume of a sphere of radius 7.5, using the formula <sup>4</sup>/<sub>3</sub> &pi; r<sup>3</sup>:

**Input**
```
4 3 / PI * 7.5 3 ^ *
```

**Output**
```
   1:   1767.1458676442585
```

The calculation happens as follows, step by step:

Description | Input | Stack
------------|-------|-------
Enter the first number | <code>4</code> | <pre>   1:   4</pre>
Enter the second number | <code>3</code> | <pre>   2:   4&#13;&#10;   1:   3</pre>
Divide | <code>/</code> | <pre>   1:   1.3333333333333333</pre>
Enter the symbolic constant &pi; | <code>PI</code> | <pre>   2:   1.3333333333333333&#13;&#10;   1:   3.141592653589793</pre>
Multiply | <code>*</code> | <pre>   1:   4.1887902047863905</pre>
Enter the radius | <code>7.5</code> | <pre>   2:   4.1887902047863905&#13;&#10;   1:   7.5</pre>
Enter the exponent | <code>3</code> | <pre>   3:   4.1887902047863905&#13;&#10;   2:   7.5&#13;&#10;   1:   3</pre>
Exponentiate | <code>^</code> | <pre>   2:   4.1887902047863905&#13;&#10;   1:   421.875</pre>
Multiply | <code>*</code> | <pre>   1:   1767.1458676442585</pre>


A feature of postfix notation is that parentheses are never required. As long as enough stack levels are available (NRPL has a conceptually infinite stack) calculations can be of any arbitrary complexity.

## Installation

`npm install nrpl`

## Calling

```javascript

const NRPL = require('nrpl');

async function runInterpreter (script) {

	const nrpl = new NRPL(/*<Global Variable Getter>,*/ /*<Global Variable Setter>,*/ /*<Allow locals?>*/);
	
	return await nrpl.exec(script);
};


runInterpreter("3 2 +").then( (stack) => console.log("Three plus two equals", stack) );
// Three plus two equals [5]

```

## Getting data in and out

Data is entered into the program via two mechanisms:
 1. Direct input into the stack, as we have seen above.
 2. Via variables, as we will see in a moment.

Retrieved may be retrieved from the stack or via assignment to global variables.

## Variables

There are two types of variables: “global” and “local”, both of which may be enabled or disabled independently.

### Global variables

These variables may be used to pass data into and out of the interpreter. For example, to persist data between calls. Globals may be read only, write only, read/write, or disabled altogether.

### Local variables

These variables are used within a single execution call, their values being lost once the script returns.

## Example instantiation with arguments


```javascript

let variables = {
	UID: process.getuid(),
	GID: process.getgid(),
	PID: process.pid
}

function getVariable(name) {
	return variables.hasOwnProperty(name)
		? variables[name]
		: undefined;
}

function setVariable(name, value) {
	// For illustration, do not allow to change PID variable
	if (name && name != "PID") {
		variables[name] = value;
	}
}

// Preload the stack with some data
let stack = [
	process.cwd(),
	"Hello"
];

const script = '" user " $UID ++ SWAP !CURRENTDIR';

const nrpl = new NRPL(getVariable, setVariable, true);

nrpl.exec(script, stack).then(console.log);

console.log("VARIABLES", variables);

// When execution completes, the stack will contain a
// greeting similar to "Hello user 12345" and variables.CURRENTDIR
// will have been set to the processes' current working directory.

```

## More information

For a full description of NRPL, please refer to the [specification](././doc/nrpl.md "NRPL specification").
