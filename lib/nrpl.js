/**
 * @module NRPL — Navlost Reverse Polish Language
 */

var Q = require('q');

var Lexer = require('lex');

var lexer = new Lexer;

// Optional dependencies
try {
	var moment = require('moment');
} catch (e) {
	if (e.code !== "MODULE_NOT_FOUND") {
		console.error(e);
	} else {
// 		console.warn("No moment");
	}
}

try {
	var GeographicLib = require('geographiclib');
	var geod = GeographicLib.Geodesic.WGS84;
} catch (e) {
	if (e.code !== "MODULE_NOT_FOUND") {
		console.error(e);
	} else {
// 		console.warn("No GeographicLib");
	}
}

try {
	var Iconv = require('iconv').Iconv;
	var iconvAscii = new Iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE');
} catch (e) {
	if (e.code !== "MODULE_NOT_FOUND") {
		console.error(e);
	} else {
// 		console.warn("No Iconv");
	}
}

function lexerRules (lexer) {

	// Match token separators (whitespace and semicolons)
	lexer.addRule(/[\s;]+/, function () {
		// Discard
	});
	
	// Match a comment.
	// Comments go either between two § signs or
	// between an § sign and a newline.
	lexer.addRule(/§([^§\n]*)[§\n]?/, function (lexeme, name) {
		this.yytext = name; // No real reason for storing the comment
		return "COMMENT";
	});

	// Match a variable name
	lexer.addRule(/\$(\w+)/, function (lexeme, name) {
		this.yytext = name;
		return "VARIABLE";
	});

	// Match a variable definition
	lexer.addRule(/!(\w+)/, function (lexeme, name) {
		this.yytext = name;
		return "VARDEF";
	});

	// Match a local variable name
	lexer.addRule(/\$\$(\w+)/, function (lexeme, name) {
		this.yytext = name;
		return "LOCALVARIABLE";
	});

	// Match a local variable definition
	lexer.addRule(/!!(\w+)/, function (lexeme, name) {
		this.yytext = name;
		return "LOCALVARDEF";
	});

	// Match a string
	lexer.addRule(/"([^"]*)"/, function (lexeme, string) {
		this.yytext = string;
		return "STRING";
	});

	// Match a number
	lexer.addRule(/[+-]?\d+(\.\d*)?|[+-]?\.\d+/, function (lexeme) {
		this.yytext = parseFloat(lexeme);
		return "NUMBER";
	});

	if (moment) {
		// Match a timestamp
		lexer.addRule(/@([^@]+)@/, function (lexeme, tstamp) {
			this.yytext = new moment(tstamp)
			return "DATETIME";
		});
	}

	// Match commands. Constants
	lexer.addRule(/(pi|𝜋|𝛑|e|ℇ)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Arithmetic operations
	lexer.addRule(/(\+|add|-|\*|×|\/|÷|±|inv|abs|rem|pow|\^|sqrt|√|sq|²|tofixed|→fix)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Trigonometric operations (hyperbolic)
	lexer.addRule(/(cosh|sinh|tanh|acosh|asinh|atanh)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Trigonometric operations
	lexer.addRule(/(cos|sin|tan|acos|asin|atan2|atan|hypothenuse|hypot)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Logarithms
	lexer.addRule(/(ln|log)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Angle conversions
	lexer.addRule(/(r→d|d→r)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Arithmetic comparison
	lexer.addRule(/(eq|ne|lt|gt|le|ge)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
	lexer.addRule(/(=|≠|<|>|≤|≥)/, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Aggregate commands
	lexer.addRule(/(max|min|sum|∑|prod|∏|count)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. String
	lexer.addRule(/(empty|substr|lower|upper)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	if (iconvAscii) {
		// Match commands. Code folding
		lexer.addRule(/(fold)/i, function (lexeme, command) {
			return command.toUpperCase();
		});
	}

	// Match commands. Regular expression
	lexer.addRule(/(regexp|rxtest|rxexec)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	if (moment) {
		// Match commands. Date/time
		lexer.addRule(/(asdatetime|utc|unixoffset|unix|now|dtformat|iso8601)/i, function (lexeme, command) {
			return command.toUpperCase();
		});
	}

	// Match commands. Logic
	lexer.addRule(/(and|or|xor|not|true|false)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
	lexer.addRule(/(∧|∨|⊻|⊕|¬|true|false)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	if (geod) {
		// Match commands. Geodetic computations (WGS84)
		lexer.addRule(/(geod|invgeod)/i, function (lexeme, command) {
			return command.toUpperCase();
		});
	}

	// Match commands. Conditionals
	lexer.addRule(/(if|else|then)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Postfix conditionals
	lexer.addRule(/(ifte|ift)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Variable
	lexer.addRule(/(exists)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Index
	lexer.addRule(/(index)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Array/Object
	lexer.addRule(/(get|set|→list|>list|list→|list>|object|array|list|global|local|each|sort|reverse|indexof)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
	
	// Match commands. Array/Object (car, cdr)
	lexer.addRule(/(c[ad]{1,7}r)/i, function (lexeme, command) {
		this.yytext = command.toUpperCase();
		return "CARCDR";
	});
	
	// Match commands. Sets
	lexer.addRule(/(newset|∅|clearset|→set|set→)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
	lexer.addRule(/(contains|∋|∍|elementof|∈|∊|∌|∉)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
	lexer.addRule(/(sintersect|∩|⋂|sunion|∪|⋃)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
	lexer.addRule(/(sminus|scomplement|∖)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
	lexer.addRule(/(sdiff|∆|sprod)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
	lexer.addRule(/(subsetof|⊂|⊆|supersetof|⊃|⊇)/i, function (lexeme, command) {
		return command.toUpperCase();
	});

	// Match commands. Stack manipulation
	lexer.addRule(/(depth|clear|dropn|drop|↓|dupn|dup2|dup|↑|swap|rot|rolld|roll|over|pick)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
	
	// Match commands. Looping
	lexer.addRule(/(while|repeat)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
	
	// Match definition statements
	// Supports up to five levels of recursion
	// Single-level match: /«(.*)»/
	lexer.addRule(/«((?:«(?:«(?:«(?:«.*?»|.)*?»|.)*?»|.)*?»|.)*?)»/, function (lexeme, literal) {
		this.yytext = literal;
		return "LITERAL";
	});
	
	lexer.addRule(/(eval)/i, function (lexeme, command) {
		return command.toUpperCase();
	});
}

class NRPLLiteral {
	
	constructor (expr, stack, parent) {
		this.expr = expr;
		this.stack = stack;
		this.nrpl = new NRPL(parent.getVar, parent.setVar, parent.allowLocals);
		this.nrpl.locals = parent.locals;
	};
	
	eval () {
		return this.nrpl.exec(this.expr, this.stack);
	}
	
	toString () {
		return "<Code Literal>";
	}
	
	toJSON () {
		return "« " + this.expr + " »";
	}
	
	inspect () {
		return "« " + this.expr + " »";
	}
	
}

class NRPL {
	
	constructor (getVar, setVar, allowLocals) {
		this.lexer = new Lexer;
		this.global = {};
		this.index = null;
		this.getVar = undefined;
		this.setVar = undefined;
		
		this.locals = {};
		this.allowLocals = allowLocals || false;
		
		this.getVar = (typeof getVar === "function" ? getVar : undefined);
		this.setVar = (typeof setVar === "function" ? setVar : undefined);
		
		lexerRules(this.lexer);
	};
	
	getLocalVar (name) {
		return this.locals[name];
	}
	
	setLocalVar (name, value) {
		if (typeof value === "undefined") {
			delete this.locals[name];
		} else {
			this.locals[name] = value;
		}
	}
	
	/**
	 * Process an NRPL expression.
	 * 
	 * @arg txt {String}    The expression to process.
	 * @arg _stack {Array?} The initial stack.
	 * 
	 * @returns A promise.
	 * Resolves to the resulting stack.
	 * Rejects with error.
	 */
	exec (txt, _stack) {
		var deferred = Q.defer();
		
		var lexer = this.lexer;
		var self = this;
		var token;
		var stack = _stack || [];
		var condition = undefined;
		
		function swap () {
			if (stack.length > 1) {
				var a = stack[stack.length-2];
				stack[stack.length-2] = stack[stack.length-1];
				stack[stack.length-1] = a;
			}
		}
		
		function evaluate (expr) {
			if (expr instanceof NRPLLiteral) {
				return expr.eval();
			} else if (typeof expr === "function") {
				return Q(expr(stack));
			} else {
				stack.push(expr);
				return Q(stack);
			}
		}
		
		// Prepare txt for lexer.
		let expr = (txt||"")
			.replace(/^#!.*\n/, "")              // Remove hashbang, if present
			.replace(/§([^\n]*\n)/g, "§$1§")     // Always end comments with §
			.replace(/[\n\r]/g, " ");            // One-line the script
		
		lexer.setInput(expr);
		
		Q.spawn(function* stackProcessor () {
			try {
				while (token = lexer.lex()) {
					if (condition === false && token != "ELSE" && token != "THEN" ) {
						continue;
					}
					switch(token) {
						case "COMMENT":
							yield stack;
							break;
						case "STRING":
						case "NUMBER":
						case "DATETIME":
							stack.push(lexer.yytext);
							yield stack;
							break;
						case "VARIABLE":
							if (typeof self.getVar === "function") {
								yield (new Promise(function (resolve, reject) {
									Q(self.getVar(lexer.yytext)).done(value => {
										stack.push(value);
										resolve(stack);
									}, err => {
										stack.push(undefined);
										resolve(stack);
									});
								}));
							} else {
								yield stack;
							}
							break;
						case "VARDEF":
							if (typeof self.setVar === "function") {
								yield (new Promise(function (resolve, reject) {
									Q(self.setVar(lexer.yytext, stack.pop())).done(() => {
										resolve(stack);
									}, err => {
										resolve(stack);
									});
								}));
							} else {
								yield stack;
							}
							break;
						case "LOCALVARIABLE":
							if (self.allowLocals) {
								let v = self.getLocalVar(lexer.yytext);
								if (v instanceof NRPLLiteral) {
									yield evaluate(v);
								} else {
									stack.push(self.getLocalVar(lexer.yytext));
								}
							}
							yield stack;
							break;
						case "LOCALVARDEF":
							if (self.allowLocals) {
								self.setLocalVar(lexer.yytext, stack.pop());
							}
							yield stack;
							break;
						case "OBJECT":
							stack.push({}); // Create an empty object
							yield stack;
							break;
						case "ARRAY":
						case "LIST":
							stack.push([]); // Create an empty array
							yield stack;
							break;
							
						case "GLOBAL":
							if (typeof self.global !== "undefined" && self.global !== null) {
								stack.push(global);
							}
							yield stack;
							break;
						case "LOCAL":
							if (typeof self.locals !== "undefined" && self.locals !== null) {
								stack.push(self.locals);
							}
							yield stack;
							break;
							
						// Commands follow
						case "PI":
						case "𝜋":
						case "𝛑":
							stack.push(Math.PI);
							yield stack;
							break;
						case "E":
						case "ℇ":
							stack.push(Math.E);
							yield stack;
							break;
							
						case "+":
						case "ADD": // Alternative to avoid escaping in browser command lines
							swap(); // For strings, etc.
							stack.push(stack.pop() + stack.pop());
							yield stack;
							break;
						case "-":
							swap();
							stack.push(stack.pop() - stack.pop());
							yield stack;
							break;
						case "*":
						case "×":
							stack.push(stack.pop() * stack.pop());
							yield stack;
							break;
						case "/":
						case "÷":
							swap();
							stack.push(stack.pop() / stack.pop());
							yield stack;
							break;
						case "±":
						case "INV":
							stack.push(-stack.pop());
							yield stack;
							break;
						case "ABS":
							stack.push(Math.abs(stack.pop()));
							yield stack;
							break;
						case "REM":
							swap();
							stack.push(stack.pop() % stack.pop());
							yield stack;
							break;
						case "POW":
						case "^":
							swap();
							stack.push(Math.pow(stack.pop(), stack.pop()));
							yield stack;
							break;
						case "SQ":
						case "²":
							stack.push(Math.pow(stack.pop(), 2));
							yield stack;
							break;
						case "SQRT":
						case "√":
							stack.push(Math.sqrt(stack.pop()));
							yield stack;
							break;
						case "TOFIXED":
						case "→FIX":
							swap();
							stack.push(stack.pop().toFixed(stack.pop()));
							yield stack;
							break;

						case "COS":
							stack.push(Math.cos(stack.pop()));
							yield stack;
							break;
						case "SIN":
							stack.push(Math.sin(stack.pop()));
							yield stack;
							break;
						case "TAN":
							stack.push(Math.tan(stack.pop()));
							yield stack;
							break;
						case "ACOS":
							stack.push(Math.acos(stack.pop()));
							yield stack;
							break;
						case "ASIN":
							stack.push(Math.asin(stack.pop()));
							yield stack;
							break;
						case "ATAN":
							stack.push(Math.atan(stack.pop()));
							yield stack;
							break;
						case "ATAN2":
							stack.push(Math.atan2(stack.pop(), stack.pop()));
							yield stack;
							break;
						case "HYPOT":
						case "HYPOTHENUSE":
							stack.push(Math.hypot(stack.pop(), stack.pop()));
							yield stack;
							break;
						case "COSH":
							stack.push(Math.cosh(stack.pop()));
							yield stack;
							break;
						case "SINH":
							stack.push(Math.sinh(stack.pop()));
							yield stack;
							break;
						case "TANH":
							stack.push(Math.tanh(stack.pop()));
							yield stack;
							break;
						case "ACOSH":
							stack.push(Math.acosh(stack.pop()));
							yield stack;
							break;
						case "ASINH":
							stack.push(Math.asinh(stack.pop()));
							yield stack;
							break;
						case "ATANH":
							stack.push(Math.atanh(stack.pop()));
							yield stack;
							break;
							
						case "LN":
							stack.push(Math.log(stack.pop()));
							yield stack;
							break;
						case "LOG":
							stack.push(Math.log10(stack.pop()));
							yield stack;
							break;

						case "R→D":
							stack.push(stack.pop()*180/Math.PI);
							yield stack;
							break;
						case "D→R":
							stack.push(stack.pop()*Math.PI/180.0);
							yield stack;
							break;
							
						case "EQ":
						case "=":
							stack.push(stack.pop() == stack.pop());
							yield stack;
							break;
						case "NE":
						case "≠":
							stack.push(stack.pop() != stack.pop());
							yield stack;
							break;
						case "LT":
						case "<":
							swap();
							stack.push(stack.pop() < stack.pop());
							yield stack;
							break;
						case "GT":
						case ">":
							swap();
							stack.push(stack.pop() > stack.pop());
							yield stack;
							break;
						case "LE":
						case "≤":
							swap();
							stack.push(stack.pop() <= stack.pop());
							yield stack;
							break;
						case "GE":
						case "≥":
							swap();
							stack.push(stack.pop() >= stack.pop());
							yield stack;
							break;
							
						case "EMPTY":
							stack.push(String(stack.pop()).length === 0);
							yield stack;
							break;
						case "SUBSTR":
							stack.push(String(stack.pop()).indexOf(stack.pop()) !== -1);
							yield stack;
							break;
						case "LOWER":
							stack.push(String(stack.pop()).toLowerCase());
							yield stack;
							break;
						case "UPPER":
							stack.push(String(stack.pop()).toUpperCase());
							yield stack;
							break;
						case "FOLD":
							stack.push(iconvAscii.convert(String(stack.pop())));
							yield stack;
							break;
							
						case "REGEXP": {
							var argument = stack.pop();
							if (Array.isArray(argument)) {
								stack.push(new RegExp(argument[0], argument[1]));
							} else {
								stack.push(new RegExp(argument));
							}
						}
							yield stack;
							break;
							
						case "RXTEST":
							stack.push(stack.pop().test(stack.pop()));
							yield stack;
							break;
							
						case "RXEXEC": {
							let res = stack.pop().exec(stack.pop());
							if (res) {
								res = res.slice(0, res.length);
							}
							stack.push(res);
							yield stack;
							break;
						}
							
						case "ASDATETIME":
							stack.push(moment(stack.pop()));
							yield stack;
							break;
						case "UTC":
							stack.push(moment.utc(stack.pop()));
							yield stack;
							break;
						case "UNIXOFFSET":
							stack.push(moment(stack.pop()).valueOf());
							yield stack;
							break;
						case "UNIX": {
							let arg = stack.pop();
							if (arg instanceof moment) {
								// To unix epoch
								stack.push(arg.unix());
							} else {
								stack.push(moment.unix(arg));
							}
							yield stack;
							break;
						}
						case "NOW":
							stack.push(moment.utc());
							yield stack;
							break;
						case "DTFORMAT":
							swap();
							stack.push(moment(stack.pop()).format(stack.pop()));
							yield stack;
							break;
							
						case "ISO8601":
							stack.push(moment(stack.pop()).toISOString());
							yield stack;
							break;
							
						case "MIN":
							if (Array.isArray(stack[stack.length-1])) {
								stack.push(Math.min.apply(null, stack.pop()));
							} else {
								stack.push(Math.min(stack.pop(), stack.pop()));
							}
							yield stack;
							break;
						case "MAX":
							if (Array.isArray(stack[stack.length-1])) {
								stack.push(Math.max.apply(null, stack.pop()));
							} else {
								stack.push(Math.max(stack.pop(), stack.pop()));
							}
							yield stack;
							break;
						case "SUM":
						case "∑":
							stack.push(stack.pop().reduce(function (prev, curr) { return prev + curr }, 0));
							yield stack;
							break;
						case "PROD":
						case "∏":
							stack.push(stack.pop().reduce(function (prev, curr) { return prev * curr }, 1));
							yield stack;
							break;
						case "COUNT": {
							let e = stack.pop();
							if (e.length !== undefined) {
								stack.push(e.length);
							} else if (e.size !== undefined) {
								stack.push(e.size);
							} else {
								stack.push(undefined);
							}
							yield stack;
							break;
						}
							
						case "∧":
						case "AND": {
							let a = stack.pop(), b = stack.pop();
							stack.push(a && b);
							yield stack;
							break;
						}
						case "∨":
						case "OR": {
							let a = stack.pop(), b = stack.pop();
							stack.push(a || b);
							yield stack;
							break;
						}
						case "⊻":
						case "⊕":
						case "XOR":
							stack.push(!stack.pop() != !stack.pop());
							yield stack;
							break;
						case "¬":
						case "NOT":
							stack.push(!stack.pop());
							yield stack;
							break;
						case "TRUE":
							stack.push(true);
							yield stack;
							break;
						case "FALSE":
							stack.push(false);
							yield stack;
							break;
							
						case "GEOD":
							// Stack: [ distance, bearing, longitude, latitude ]
							stack.push(geod.Direct(stack.pop(), stack.pop(), stack.pop(), stack.pop()));
							yield stack;
							break;
						case "INVGEOD":
							// Stack: [ lon1, lat1, lon0, lat0 ]
							stack.push(geod.Inverse(stack.pop(), stack.pop(), stack.pop(), stack.pop()));
							yield stack;
							break;
							
						case "IF":
							if (stack.pop()) {
								condition = true;
							} else {
								condition = false;
							}
							yield stack;
							break;
						case "ELSE":
							condition = !condition;
							yield stack;
							break;
						case "THEN":
							condition = undefined;
							yield stack;
							break;
							
						case "IFT": {
							let expr = stack.pop();
							let cond = stack.pop();
							yield evaluate(cond).then(v => {
								if (stack.pop()) {
									return evaluate(expr);
								}
							});
							break;
						}
						
						case "IFTE": {
							let exprF = stack.pop();
							let exprT = stack.pop();
							let cond = stack.pop();
							yield evaluate(cond).then(v => {
								if (stack.pop()) {
									return evaluate(exprT);
								} else {
									return evaluate(exprF);
								}
							});
							break;
						}
							
						case "EXISTS":
							stack.push(typeof stack.pop() !== "undefined");
							yield stack;
							break;
							
						case "INDEX":
							stack.push(self.index);
							yield stack;
							break;
							
						case "GET":
							stack.push(stack.pop()[stack.pop()]);
							yield stack;
							break;
							
						case "SET": {
							var ar = stack.pop();
							if (Array.isArray(ar)) {
								ar = ar.slice();
							}
							ar[stack.pop()] = stack.pop();
							stack.push(ar);
							yield stack;
							break;
						}
						
						case "EACH":
							if (Array.isArray(stack[stack.length-2])) {
								var key = stack.pop();
								var ar = stack.pop();
								var result = Array();
								ar.forEach(function (item) {
									if (item.hasOwnProperty(key)) {
										result.push(item[key]);
									}
								});
								stack.push(result);
							}
							yield stack;
							break;
							
						case "INDEXOF":
							stack.push(stack.pop().indexOf(stack.pop()));
							yield stack;
							break;

						case "CARCDR": {
							for (let c of [...lexer.yytext.slice(1, -1)].reverse()) {
								switch (c) {
									case "A":
										stack.push(stack.pop()[0]);
										break;
									case "D":
										stack.push(stack.pop().slice(1));
										break;
								}
							}
							yield stack;
							break;
						}
							
						case "REVERSE":
							stack.push(stack.pop().reverse());
							yield stack;
							break;
							
						case "SORT":
							stack.push(stack.pop().sort( (a, b) => {
								if (typeof a === "number" && typeof b === "number") {
									return a - b;
								} else {
									return a == b? 0 : a < b ? -1 : 1;
								}
							}));
							yield stack;
							break;
						
						case "→LIST":
						case ">LIST": {
							let n = stack.pop();
							if (n instanceof Set) {
								stack.push([... n.values()])
							} else /*if (typeof n === 'number')*/ {
								stack.splice(-n, n, stack.slice(-n));
							}
							yield stack;
							break;
						}
						case "SET→":
						case "LIST→":
						case "LIST>": {
							let n = stack.pop();
							let l = stack.length;
							if (n[Symbol.iterator]) {
								for (let v of n) {
									stack.push(v);
								}
								stack.push(n.length || n.size || (stack.length - l));
							} else {
								console.error(new Error("Non decomposable element"));
							}
							yield stack;
							break;
						}
							
							
						case "NEWSET":
						case "∅":
							stack.push(new Set());
							yield stack;
							break;
						case "CLEARSET": {
							let s = stack.pop();
							s.clear();
							stack.push(s);
							yield stack;
							break;
						}
						case "→SET": {
							let n = stack.pop();
							if (typeof n === 'number') {
								stack.splice(-n, n, new Set(stack.slice(-n)));
							} else { // Arrays, sets of sets, etc.
								stack.push(new Set(n));
							}
							yield stack;
							break;
						}
						case "CONTAINS":
						case "∋":
						case "∍":
							swap();
							// Fallthrough
						case "ELEMENTOF":
						case "∈":
						case "∊":
							stack.push(stack.pop().has(stack.pop()))
							yield stack;
							break;
						case "∌":
							swap();
						case "∉":
							stack.push(!stack.pop().has(stack.pop()))
							yield stack;
							break;
						case "SINTERSECT":
						case "∩":
						case "⋂": {
							let b = stack.pop(), a = stack.pop();
							if (!b.has) {
								b = new Set(b);
							}
							stack.push(new Set([...a].filter(x => b.has(x))));
							yield stack;
							break;
						}
						case "SUNION":
						case "∪":
						case "⋃": {
							let s = [];
							[stack.pop(), stack.pop()].forEach(e => {
								if (e[Symbol.iterator]) {
									s.push(...e);
								} else {
									s.push(e);
								}
							});
							stack.push(new Set(s));
							yield stack;
							break;
						}
						case "SMINUS":
						case "SCOMPLEMENT":
						case "∖": {
							let b = stack.pop(), a = stack.pop();
							stack.push(new Set([...a].filter(x => !b.has(x))));
							yield stack;
							break;
						}
						case "SDIFF":
						case "∆": {
							let b = stack.pop(), a = stack.pop();
							let a1 = new Set([...a].filter(x => !b.has(x)));
							let b1 = new Set([...b].filter(x => !a.has(x)));
							stack.push(new Set([...a1, ...b1]));
							yield stack;
							break;
						}
						case "SPROD": {
							let b = stack.pop(), a = stack.pop();
							if (!a[Symbol.iterator]) {
								a = [a];
							}
							if (!b[Symbol.iterator]) {
								b = [b];
							}
							let s = new Set();
							for (let i of a) {
								for (let j of b) {
									s.add([i, j]);
								}
							}
							stack.push(s);
							yield stack;
							break;
						}
						case "SUPERSETOF":
						case "⊃":
						case "⊇":
							swap();
							// Fallthrough
						case "SUBSETOF":
						case "⊂":
						case "⊆": {
							let b = stack.pop(), a = stack.pop();
							stack.push([...b].every(x => a.has(x)));
							yield stack;
							break;
						}

							
						case "DEPTH":
							stack.push(stack.length);
							yield stack;
							break;
						case "CLEAR":
							stack.length = 0;
							yield stack;
							break;
						case "DROP":
						case "↓":
							stack.pop();
							yield stack;
							break;
						case "DROPN":
							stack.splice(-Math.abs(stack.pop()));
							yield stack;
							break;
						case "DUP":
						case "↑":
							stack.push(stack[stack.length-1]);
							yield stack;
							break;
						case "DUP2":
							stack.push(...stack.slice(-2));
							yield stack;
							break;
						case "DUPN":
							stack.push(...stack.slice(-Math.abs(stack.pop())));
							yield stack;
							break;
						case "SWAP":
							swap();
							yield stack;
							break;
						case "ROT":
							stack.push(stack.splice(-3, 1)[0]);
							yield stack;
							break;
						case "ROLL":
							if (stack.length > 1) {
								stack.push(stack.splice(-stack.pop(), 1)[0]);
							}
							yield stack;
							break;
						case "ROLLD":
							if (stack.length > 1) {
								stack.splice(-stack.pop(), 0, stack.pop());
							}
							yield stack;
							break;
						case "OVER":
							stack.push(stack[stack.length-2]);
							yield stack;
							break;
						case "PICK":
							stack.push(stack[stack.length-stack.pop()]);
							yield stack;
							break;
							
						case "LITERAL": {
							let expr = lexer.yytext;
							stack.push(new NRPLLiteral(expr, stack, self));
							yield stack;
						}
						break;
						
						case "EVAL": {
							yield evaluate(stack.pop());
							break;
						}
						
						case "WHILE": {
							let expr = stack.pop();
							let cond = stack.pop();
							let maxIter = 5000, iter = 0;
							
							while ((yield cond.eval()) && stack.pop() && (iter++ < maxIter)) {
								yield expr.eval();
							}
							
							if (iter >= maxIter) {
								throw new Error("Too many iterations. WHILE loop has been aborted");
							}
							
							yield stack;
							break;
						}
						
						case "REPEAT": {
							let expr = stack.pop();
							let cond = Number(stack.pop());
							let maxIter = 5000, iter = 0;
							
							if (cond < maxIter) {
								for (let n = 0; n < cond; n++) {
									yield expr.eval();
								}
							} else {
								throw new Error("Too many iterations. REPEAT loop will not be run");
							}
							
							yield stack;
							break;
						}
							
						default:
							throw new Error("Unrecognised token: " + token);
					}
				}
			} catch (e) {
				e.expr = expr;
				deferred.reject(e);
			}
			
			deferred.resolve(stack);
		});
		
		return deferred.promise;
	}
	
	/**
	 * Process an expression and immediately
	 * return the bottommost value of the stack.
	 * 
	 * Starts with an empty stack.
	 * 
	 * @arg txt The expression to process.
	 * 
	 * @return A promise.
	 * Resolves to the value on stack level 1.
	 * Rejects with error.
	 */
	pop (txt) {
		return this.exec(txt).then(stack => stack.pop());
	}
	
	/**
	 * Report the type and representation of an object.
	 */
	type (obj) {
		var deferred = Q.defer();
		
		let item = {};
		if (typeof obj === "string") {
			item.value = String(obj);
			item.type = "string";
			deferred.resolve(item);
		} else if (typeof obj === "undefined") {
			item.value = obj;
			item.type = "undefined";
			deferred.resolve(item);
		} else if (obj instanceof NRPLLiteral) {
			item.value = obj.inspect();
			item.type = "code literal";
			deferred.resolve(item);
		} else if (obj != null && obj.hasOwnProperty("_isAMomentObject") && obj._isAMomentObject) {
			item.value = "@"+obj.toISOString()+"@";
			item.type = "timestamp";
			deferred.resolve(item);
		} else if (obj === true || obj === false) {
			item.value = obj;
			item.type = "boolean";
			deferred.resolve(item);
		} else if (obj === null) {
			item.value = obj;
			item.type = "null";
			deferred.resolve(item);
		} else if (typeof obj === "number") {
			if (Number.isFinite(obj)) {
				item.value = obj;
			} else {
				item.value = obj.toString();
			}
			item.type = "number";
			deferred.resolve(item);
		} else if (Array.isArray(obj)) {
			item.value = [];
			let self = this;
			item.type = "array";
			Q.spawn(function* () {
				for (let i of obj) {
					let v = yield self.type(i);
					item.value.push(v);
				}
				deferred.resolve(item);
			});
		} else if (obj instanceof Set) {
			item.value = [];
			let self = this;
			item.type = "set";
			Q.spawn(function* () {
				for (let i of obj) {
					let v = yield self.type(i);
					item.value.push(v);
				}
				deferred.resolve(item);
			});
		} else if (typeof obj === "object") {
			item.value = {};
			let self = this;
			item.type = "object";
			Q.spawn(function* () {
				for (let k in obj) {
					item.value[k] = yield self.type(obj[k]);
				}
				deferred.resolve(item);
			});
		} else {
			item.value = obj;
			item.type = typeof obj;
			deferred.resolve(item);
		}
		
		return deferred.promise;
	}
	
	/**
	 * Dumps the stack to a JSON object.
	 */
	dump (stack) {
		let output = [];
		let n = stack.length;
		while (n > 0) {
			let obj = stack[stack.length-n];
			let item = this.type(obj);
			item.level = n;
			output.push(item);
			n--;
		}
		
		if (!output.length) {
			return Q({
				value: [],
				type: "stack"
			});
		};
		
		return Q.all(output).then(v => {
			return {
				value: v,
				type: "stack"
			}
		});
	}
	
	/**
	 * Converts a stack dump back into an NRPL expression.
	 */
	undump (obj) {
		var expr = [];
		if (Array.isArray(obj)) {
			expr.push(obj.map(i => this.undump(i)).join("\n"));
		} else {
			switch (obj.type) {
				case "object":
					expr.push(" OBJECT ");
					for (let k in obj.value) {
						expr.push(`${this.undump(obj.value[k])} "${k}" ROT SET`);
					}
					break;
				case "array":
				case "stack":
					obj.value.map(i => expr.push(this.undump(i)));
					expr.push(obj.value.length, " →LIST");
					break;
				case "set":
					obj.value.map(i => expr.push(this.undump(i)));
					expr.push(obj.value.length, "→SET");
					break;
				case "string":
					expr.push(`"${obj.value}"`);
					break;
				default:
					expr.push(obj.value);
					break;
			}
		}
		
		return expr.join("\n");
	}
	
	/**
	 * Print stack
	 */
	printStack (dump, indChar) {
		var ic = indChar || "\t";
		
		function pr(obj, indent) {
			let str = "";
			switch (obj.type) {
				case "stack": {
					let n = obj.value.length;
					if (n) {
						obj.value.forEach(v => {
							let item = pr(v, indent+1);
							let level = " ".repeat(4 - String(n).length)+n;
							str += `${level}:${item.replace(/\n/g, "\n·····").replace(/·····$/, "")}`;
							n--;
						});
					}
					break;
				}
				case "array": {
					if (obj.value.length) {
						str += `${ic.repeat(indent)}[\n`;
						obj.value.forEach(v => {
							let item = pr(v, indent+1);
							str += `${item}`;
						});
						str += `${ic.repeat(indent)}]\n`;
					} else {
						str += `${ic.repeat(indent)}[]\n`;
					}
					break;
				}
				case "set": {
					if (obj.value.length) {
						str += `${ic.repeat(indent)}{\n`;
						obj.value.forEach(v => {
							let item = pr(v, indent+1);
							str += `${item}`;
						});
						str += `${ic.repeat(indent)}}\n`;
					} else {
						str += `${ic.repeat(indent)}{}\n`;
					}
					break;
				}
				case "object": {
					let keys = Object.keys(obj.value);
					if (keys.length) {
						str += `${ic.repeat(indent)}{\n`;
						keys.forEach(k => {
							let item = pr(obj.value[k], indent+1);
							str += `${item}`;
						});
						str += `${ic.repeat(indent)}}\n`;
					} else {
						str += `${ic.repeat(indent)}{}\n`;
					}
					break;
				}
				case "string":
					str += `${ic.repeat(indent)}"${obj.value}"\n`;
					break;
				default:
					str += `${ic.repeat(indent)}${obj.value}\n`;
			}
			return str;
		};
		
		if (Array.isArray(dump)) {
			return this.dump(dump).then( d => {
				return this.printStack(d, indChar);
			});
		} else {
			return Q(pr(dump, 0));
		}
	}
	
};

module.exports = NRPL;
